import React, { Component } from 'react';
import { Alert, Image, View, Text, TouchableHighlight, ImageBackground, StatusBar } from 'react-native';
import firebase from 'react-native-firebase';
import { profile } from './src/styles/Styles'

export default class Profile extends Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        name: '',
        email: '',
        phone: '',
    }
    componentDidMount() {
        const { email, displayName, phoneNumber } = firebase.auth().currentUser
        this.setState({
            name: displayName,
            email: email,
            phone: phoneNumber,
        })
        if (this.state.name == null) {
            this.setState({
                name: 'Unidentified',
            })
        }
        if (this.state.phone.length == '') {
            this.setState({
                phone: 'Not Added'
            })
        }
    }
    signOut = () => {
        firebase
            .auth()
            .signOut()
            .then(Alert.alert('Sign Out Success'))
            .then(() => Container.props.navigation.navigate('landing'))
            .catch(error => this.setState({ errorMessage: error.message }))
    }
    render() {
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode='cover'>
                <StatusBar />
                <View style={profile.containerBg} />
                <View style={profile.container}>
                    <View style={profile.header}>
                        <Image source={require('./src/img/default.png')} style={profile.headerPic} />
                        <Text style={profile.headerName}>{this.state.name}</Text>
                    </View>
                    <View style={profile.body}>
                        <View style={profile.bodiCon}>
                            <Text style={profile.bodyLabel}>
                                Email :
                            </Text>
                            <View style={profile.bodyEntryCon}>
                                <Text style={profile.bodyEntryText}>
                                    {this.state.email}
                                </Text>
                            </View>
                        </View>
                        <View style={profile.bodiCon}>
                            <Text style={profile.bodyLabel}>
                                Phone :
                            </Text>
                            <View style={profile.bodyEntryCon}>
                                <Text style={profile.bodyEntryText}>
                                    {this.state.phone}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={profile.footerContainer}>
                        <View style={profile.footer}>
                            <TouchableHighlight onPress={() => this.props.navigation.navigate('changeName')} style={profile.footerCon} underlayColor='white'>
                                <Text style={profile.footerText}>
                                    Change Name
                                    </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={profile.footerOut}>
                            <TouchableHighlight onPress={this.signOut} style={profile.footerConOut} underlayColor='white'>
                                <Text style={profile.footerTextOut}>
                                    Sign Out
                                    </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        )
    }

}