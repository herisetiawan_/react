import React, { Component } from 'react';
import {
    Text,
    View,
    Alert,
    StatusBar,
    TextInput,
    ScrollView,
    ImageBackground,
    TouchableHighlight,
} from 'react-native';
import {
    register,
    basic
} from './src/styles/Styles';
import firebase from 'react-native-firebase';
import { Container } from './Navigator'

export default class Register extends Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        // first: '',
        // last: '',
        // username: '',
        email: '',
        // date: '',
        password: '',
        repass: '',
        errorMessage: null,

    }
    handleSignUp = () => {
        const { email, password, repass } = this.state
        const emailVer = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailVer.test(email) === true) {
            if (password.length >= 8) {
                if (password == repass) {
                    firebase
                        .auth()
                        .createUserWithEmailAndPassword(email.toLowerCase(), password)
                        .then(Alert.alert('Sign Up Success'))
                        .then(() => Container.props.navigation.navigate('loading'))
                        .catch(error => console.log('Error' + error))
                }
            }
        } else {
            Alert.alert('Enter valid email')
        }
        console.log('handleSignUp')
    }
    render() {
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode='cover' blurRadius={2}>
                <StatusBar backgroundColor='black' />
                <View style={register.container}>
                    <ScrollView>
                        <View style={register.headContainer}>
                            <Text style={basic.head}>
                                Welcome
                    </Text>
                            <Text style={basic.headSub}>
                                Register now for free..!!
                    </Text>
                        </View>
                        <View style={basic.form}>
                            <Text style={basic.formLabel}>
                                Email :
                            </Text>
                            <TextInput style={basic.input}
                                placeholder='Enter your email here'
                                placeholderTextColor='#cccccc'
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email}
                                keyboardType='email-address' />
                            <Text style={basic.formLabel}>
                                Password :
                            </Text>
                            <TextInput style={basic.input}
                                placeholder='Enter your password here'
                                placeholderTextColor='#cccccc'
                                onChangeText={password => this.setState({ password })}
                                value={this.state.password}
                                secureTextEntry />
                            <Text style={basic.formLabel}>
                                Re-type Password :
                            </Text>
                            <TextInput style={basic.input}
                                placeholder='Re-type your password here'
                                placeholderTextColor='#cccccc'
                                onChangeText={repass => this.setState({ repass })}
                                value={this.state.repass}
                                secureTextEntry />
                        </View>
                        <View style={basic.submit}>
                            <TouchableHighlight style={basic.submitButton} onPress={this.handleSignUp} underlayColor='#333'>
                                <View style={basic.submitTextCon}>
                                    <Text style={basic.submitText}>
                                        Register
                                </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </ScrollView>
                </View>
            </ImageBackground>
        )
    }
}