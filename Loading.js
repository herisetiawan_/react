import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet, ImageBackground, StatusBar } from 'react-native';
import firebase from 'react-native-firebase';
export default class Loading extends React.Component {
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'profile' : 'landing')
        })
    }
    static navigationOptions = {
        header: null,
    }
    render() {
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode='cover' blurRadius={2}>
                <StatusBar backgroundColor='black' />
                <View style={styles.container}>
                    <Text>Loading</Text>
                    <ActivityIndicator size="large" />
                </View>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})