import React, { Component } from 'react';
import { Alert, TextInput, Image, View, Text, TouchableHighlight, ImageBackground, StatusBar } from 'react-native';
import firebase from 'react-native-firebase';
import { change, profile } from './src/styles/Styles'

export default class ChangePhone extends Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        name: '',
        errorMessage: '',
    }
    componentDidMount() {
        const { currentUser } = firebase.auth()
        this.setState({
            name: currentUser.displayName,
        })
    }
    setName = () => {
        const { name } = this.state
        const { currentUser } = firebase.auth()
        currentUser.updateProfile({
            displayName: name,
        })
            .then(Alert.alert('Change Name Success'))
            .then(() => Container.props.navigation.navigate('loading'))
            .catch(error => this.setState({ errorMessage: error.message }))

    }
    render() {
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode='cover'>
                <StatusBar />
                <View style={profile.containerBg} />
                <View style={change.container}>
                    <Text style={change.text}>
                        Change name to :
                    </Text>
                    <TextInput onChangeText={name => this.setState({ name })} value={this.state.name} style={change.textInput} />
                    <TouchableHighlight onPress={this.setName} style={change.button}>
                        <Text style={change.buttonText}>Update</Text>
                    </TouchableHighlight>
                </View>
            </ImageBackground>
        )
    }
}