import { StyleSheet, View, Text } from 'react-native';

const landing = StyleSheet.create({
    container: {
        height: '100%',
    },
    welcomeCon: {
        paddingTop: '20%',
        marginHorizontal: '10%',
        height: '20%'
    },
    welcome: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 30,
        textShadowColor: 'black',
        textShadowRadius: 10,
        marginTop: 10
    },
    headerTextStyle: {
        marginTop: -10,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 50,
        textAlignVertical: 'center',
        color: 'white',
        textShadowColor: 'black',
        textShadowRadius: 10,
        textTransform: 'uppercase'
    },
    buttonCon: {

        borderWidth: 2,
        borderColor: 'white',
        borderRadius: 30,
        width: '50%',
        height: 50,
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center"
    },
    button: {
        height: 46,
        width: '100%',
        borderRadius: 30,
        justifyContent: "center"
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20
    },
    descContainer: {
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: '40%',
        marginBottom: '50%',
        marginHorizontal: '5%',
    },
    descText: {
        margin: 20,
        color: 'white',
        textAlign: 'justify',
        fontSize: 15,
        fontWeight: "600"

    }
})
const basic = StyleSheet.create({
    head: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 50,
        shadowColor: 'black',
        shadowRadius: 20
    },
    headSub: {
        color: 'white',
        textAlign: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        shadowColor: 'black',
        shadowRadius: 20
    },
    form: {
        paddingTop: '5%',
        marginHorizontal: '5%'
    },
    formLabel: {
        color: 'white',
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    input: {
        backgroundColor: '#000000',
        opacity: 0.7,
        borderRadius: 5,
        color: 'white'
    },
    submit: {
        paddingTop: '5%',
        flexDirection: 'row-reverse',
        marginHorizontal: '5%',
        paddingBottom: '20%'
    },
    submitButton: {
        borderWidth: 1,
        borderColor: 'white',
        height: 40,
        width: 100,
        borderRadius: 10,
    },
    submitText: {
        color: 'white',
        paddingTop: '10%'
    },
    submitTextCon: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    another: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: '10%'
    },
    anotherText: {
        color: 'white'
    },
})
const login = StyleSheet.create({
    container: {
        flex: 1,
    },
    headContainer: {
        paddingTop: '30%'
    },
});
const register = StyleSheet.create({
    container: {

    },
    headContainer: {
        paddingTop: '5%'
    },

})
const profile = StyleSheet.create({
    containerBg: {
        width: '90%',
        height: '90%',
        backgroundColor: 'white',
        margin: '5%',
        opacity: 0.5,
        position: 'absolute',
        borderTopStartRadius: 50,
        borderBottomEndRadius: 50
    },
    container: {
        width: '90%',
        height: '90%',
        margin: '5%'
    },
    header: {
        backgroundColor: 'black',
        height: 100,
        flexDirection: 'row',
        opacity: 0.75,
        width: '100%',
        borderTopStartRadius: 50
    },
    headerPic: {
        backgroundColor: 'white',
        width: 70,
        height: 70,
        borderRadius: 50,
        margin: 15
    },
    headerName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
        marginVertical: 30,
    },
    body: {
        marginHorizontal: 15
    },
    bodyCon: {

    },
    bodyLabel: {
        color: '#555555',
        fontWeight: "bold",
        marginVertical: 5,
        fontSize: 15
    },
    bodyEntryCon: {
        backgroundColor: '#333',
        height: 50,
        borderRadius: 10,
        justifyContent: 'center'
    },
    bodyEntryText: {
        color: 'white',
        fontSize: 20,
        marginLeft: 15
    },
    footerContainer: {
        justifyContent:'flex-end',
        height: '50%'
    },
    footer: {
        borderWidth: 1,
        borderRadius: 30,
        borderColor: "black",
        width: '50%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    footerCon: {
        height: 48,
        width: '100%',
        borderRadius: 30,
        justifyContent: 'center',
    },
    footerText: {
        textAlign: 'center',
        color: 'black',
        fontSize: 20

    },
    footerOut: {
        borderWidth: 2,
        borderRadius: 30,
        borderColor: "red",
        width: '50%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center'
    },
    footerConOut: {
        height: 46,
        width: '100%',
        borderRadius: 30,
        justifyContent: 'center',
    },
    footerTextOut: {
        textAlign: 'center',
        color: 'red',
        fontWeight: 'bold',
        fontSize: 20

    }
})
const change = StyleSheet.create({
    container : {
        width: '90%',
        height: '90%',
        margin: '5%',
        justifyContent: 'center'
    },
    text: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#333',
        paddingBottom: 10,
        fontSize: 20
    },
    textInput: {
        backgroundColor: '#333',
        opacity: 0.7,
        color: 'white',
        width: '80%',
        alignSelf: 'center',
        borderRadius: 10
    },
    button: {
        borderColor: '#333',
        borderWidth: 1,
        alignSelf: 'center',
        width: '50%',
        margin: 20,
        height: 50,
        borderRadius: 50,
        justifyContent: 'center'
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 20,
    }
})
export { landing, login, register, basic, profile, change }