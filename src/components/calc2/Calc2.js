import React, { Component } from 'react';
import { StyleSheet, View, Text, Dimensions, TextInput, Button, Alert } from 'react-native';

export default class Calc2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            first: '',
            second: '',
        }
    }

    _plus() {
        if (isNaN(this.state.first) || isNaN(this.state.second)) {
            Alert.alert('Harap masukan angka..!!')
        } else {
            Alert.alert('Total', (this.state.first + this.state.second).toString());
        }
    }
    _min() {
        if (isNaN(this.state.first) || isNaN(this.state.second)) {
            Alert.alert('Harap masukan angka..!!')
        } else {
            Alert.alert('Total', (this.state.first - this.state.second).toString());
        }
    }
    _mul() {
        if (isNaN(this.state.first) || isNaN(this.state.second)) {
            Alert.alert('Harap masukan angka..!!')
        } else {
            Alert.alert('Total', (this.state.first * this.state.second).toString());
        }
    }
    _div() {
        if (isNaN(this.state.first) || isNaN(this.state.second)) {
            Alert.alert('Harap masukan angka..!!')
        } else {
            Alert.alert('Total', (this.state.first / this.state.second).toString());
        }
    }

    render() {
        return (
            <View style={style.container}>
                <View style={style.innerContainer}>
                    <Text style={style.judul}>Calc2</Text>
                    <Text style={style.head}>Harap Masukan Angka...</Text>
                    <View style={style.textbox}>
                        <TextInput
                            placeholder="Masukan angka pertama" returnKeyLabel={"next"}
                            onChangeText={(text) => this.setState({ first: parseInt(text) })}
                            keyboardType='numeric'
                        />
                    </View>
                    <View style={style.textbox}>
                        <TextInput
                            placeholder="Masukan angka kedua" returnKeyLabel={"next"}
                            onChangeText={(text) => this.setState({ second: parseInt(text) })}
                            keyboardType='numeric'
                        />
                    </View>
                    <View style={style.tombolStyle}>
                        <View style={style.tombol2}>
                            <Button
                                style={style.tombol2}
                                onPress={() => this._plus()}
                                color='#a00'
                                title="+" />
                        </View>
                        <View style={style.tombol2}>
                            <Button
                                onPress={() => this._min()}
                                color='#a00'
                                title="-" />
                        </View>
                        <View style={style.tombol2}>
                            <Button
                                onPress={() => this._mul()}
                                color="#a00"
                                title="*" />
                        </View>
                        <View style={style.tombol2}>
                            <Button
                                style={style.tombol2}
                                onPress={() => this._div()}
                                color="#a00"
                                title="/" />
                        </View>
                    </View>
                </View>
            </View>

        )
    }

}

const style = StyleSheet.create({
    judul: {
        color: 'white',
        fontSize: 60,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#333'
    },
    textbox: {
        borderColor: '#a00',
        borderWidth: 2,
        borderRadius: 5,
        backgroundColor: 'white',
        marginBottom: 5,
        fontSize: 30
    },
    head: {
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white'
    },
    tombolStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between'

    },
    tombol2: {
        width: '20%',
    },
    innerContainer: {
        marginLeft: 16,
        marginRight: 16,
    }
})
