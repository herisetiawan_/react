import React, { Component } from 'react';
import { Text, View, StyleSheet, StatusBar } from 'react-native';

class BarAtas extends Component {
    render() {
        return (
            <View>
                <StatusBar backgroundColor="#008888" barStyle="light-content" />
                <View style={gaya.background}>
                    <Text style={gaya.middle}>Heri Setiawan</Text>
                </View>
            </View>
        )
    }
}

const gaya = StyleSheet.create({
    background: {
        position: "relative",
        flexDirection: "row",
        alignItems: "stretch",
        backgroundColor: "#00aaaa",
        height: 60,
        justifyContent: "center"
    },
    middle: {
        fontFamily: "arial",
        fontWeight: "bold",
        fontSize: 25,
        textAlign: "center",
        textAlignVertical: "center",
        color: "white"
    }
})

export default BarAtas;