import React from 'react'
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native'
const Button = ({ submitTodo }) => (
    <View style={styles.buttonContainer}>
        <TouchableHighlight underlayColor='#222222' style={styles.button} onPress={submitTodo}>
            <Text style={styles.submit}> Submit
</Text>
        </TouchableHighlight>
    </View>
)
const styles = StyleSheet.create({
    buttonContainer: {
        alignItems: 'flex-end'
    },
    button: {
        height: 50, 
        paddingLeft: 20, 
        paddingRight: 20,
        backgroundColor: '#333333', 
        width: 200,
        marginRight: 20, 
        marginTop: 15, 
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,.1)', 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderRadius: 5,
},
    submit: {
        color: '#aaaaaa', 
        fontWeight: '600'
    }
})
export default Button