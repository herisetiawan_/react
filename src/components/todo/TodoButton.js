import React from 'react'
import { Text, TouchableHighlight, StyleSheet } from 'react-native'
const TodoButton = ({ onPress, complete, name }) => (
    <TouchableHighlight onPress={onPress} underlayColor='#000000' style={styles.button}>
    <Text style={[
        name === 'Done' ? styles.doneButton : null,
        complete ? styles.complete : null,
        name === 'Delete' ? styles.deleteButton : null]}
    >
        {name}
    </Text>
</TouchableHighlight >
)
const styles = StyleSheet.create({
    button: {
        alignSelf: 'flex-end',
        padding: 7,
        borderColor: '#ffffff', 
        borderWidth: 1, 
        borderRadius: 4, 
        marginRight:5
    },
    text: {
        color: '#666666'
    },
    complete: {
        color: 'green',
        fontWeight: 'bold'
    },
    deleteButton: {
        color: 'red'
    },
    doneButton: {
        color: 'white'
    }
})
export default TodoButton