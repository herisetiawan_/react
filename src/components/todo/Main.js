import React, { Component } from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import Heading from './Heading'
import Input from './Input'
import Button from './Button'
import TodoList from './TodoList'
import TabBar from './TabBar'
import BarStatus from './BarStatus'
import AsyncStorage from '@react-native-community/async-storage'
let todoIndex = 0
class Main extends Component {
    constructor() {
        super()
        this.state = {
            inputValue: '', todos: [],
            type: 'All', myKey: '',
        }
        this.toggleComplete = this.toggleComplete.bind(this)
        this.deleteTodo = this.deleteTodo.bind(this)
        this.submitTodo = this.submitTodo.bind(this)
        this.setType = this.setType.bind(this)
    }
    componentDidMount(){
        return AsyncStorage.getItem('todos')
        .then(todos => todos.json())
        .then((todosJson => {
            this.setState({
                todos: todosJson
            });
        }))
        .catch(error => console.log('Error' + error))
    }
    inputChange(inputValue) {
        this.setState({ inputValue })
    }
    setType(type) {
        this.setState({ type })
    }
    submitTodo() {
        if (this.state.inputValue.match(/^\s*$/)) {
            return
        }
        const todo = {
            title: this.state.inputValue,
            todoIndex,
            complete: false
        }
        todoIndex++

        const todos = [...this.state.todos, todo]
        this.setState({ todos, inputValue: '' }, () => {
        });
        return AsyncStorage.setItem('todos',JSON.stringify(todos))
        .then(json => console.log('Success'))
        .catch(error => console.log('Error' + error))

    }
    deleteTodo(todoIndex) {
        let { todos } = this.state
        todos = todos.filter((todo) => todo.todoIndex !== todoIndex)
        this.setState({ todos })
    }
    toggleComplete(todoIndex) {
        let todos = this.state.todos
        todos.forEach((todo) => {
            if (todo.todoIndex === todoIndex) {
                todo.complete = !todo.complete
            }
        })
        this.setState({ todos })
    }
    render() {
        const { todos, inputValue, type } = this.state
        return (
            <View style={styles.container}>
                <BarStatus />
                <ScrollView keyboardShouldPersistTaps='always' style={styles.content}>
                    <Heading />
                    <Input inputValue={inputValue}
                        inputChange={(text) => this.inputChange(text)} />
                    <TodoList type={type} toggleComplete={this.toggleComplete}
                        deleteTodo={this.deleteTodo} todos={todos} />
                    <Button submitTodo={this.submitTodo} />
                </ScrollView>
                <TabBar type={type} setType={this.setType} /></View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#555555'
    },
    content: {
        flex: 1,
        paddingTop: 60
    },
    input: {
        backgroundColor: '#333333'
    }
})
export default Main