import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
const Heading = () => (
    <View style={styles.header}>
        <Text style={styles.headerText}>
            ToDo.
        </Text>
    </View>
)
const styles = StyleSheet.create({
    header: {
        marginTop: 80
    },
    headerText: {
        textAlign: 'center', 
        fontSize: 72,
        color: '#ffffff',
        fontWeight: 'bold'
    },
})
export default Heading