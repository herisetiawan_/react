import React, { Component } from 'react';
import { Text, View, StyleSheet, StatusBar } from 'react-native';

class BarStatus extends Component {
    render() {
        return (
            <View>
                <StatusBar backgroundColor="#333333" barStyle="light-content" />
            </View>
        )
    }
}

export default BarStatus;