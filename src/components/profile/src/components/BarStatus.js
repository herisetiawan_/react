import React, { Component } from 'react';
import {View,StatusBar } from 'react-native';

class BarStatus extends Component {
    render() {
        return (
            <View>
                <StatusBar backgroundColor="#888888" barStyle="light-content" />
            </View>
        )
    }
}

export default BarStatus;