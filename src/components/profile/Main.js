import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, StatusBar } from 'react-native';
import { package1 } from './src/styles/package1';
export default class App extends Component {
    render() {
        return (
            <>
                <StatusBar backgroundColor="#777777" barStyle="light-content" />
                <View style={package1.container}>
                    <View style={package1.cardContainer}>
                        <View style={package1.cardImageContainer}>
                            <Image style={package1.cardImage}
                                source={require('./src/img/profile.jpg')} />
                        </View>
                        <View>
                            <Text style={package1.cardName}>
                                Heri Setiawan
                        </Text>
                        </View>
                        <View style={package1.cardOccupationContainer}>
                            <Text style={package1.cardOccupation}>
                                React Native Developer (<Text style={package1.cardOccLvl}>Beginner</Text>)
                        </Text>
                        </View>
                        <View>
                            <Text style={package1.cardDescription}>
                                Heri is just a human who want to be really great JavaScript developer. He loves using JS to build React Native applications for iOS and Android.
                        </Text>
                        </View>
                    </View>
                </View>
            </>
        );
    }
}
