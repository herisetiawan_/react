
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Landing from './Landing';
import Loading from './Loading'
import Login from './Login';
import Register from './Register';
import Extras from './Extra';
import Profile from './Profile';
import Name from './ChangeName';

const Lander = createStackNavigator(
    {
        landing: { screen: Landing },
        extras: { screen : Extras},
        login: {screen: Login},
        register: {screen: Register},
        loading: {screen: Loading},
        profile: {screen: Profile},
        changeName: {screen: Name},
        changePhone: {screen: Name},
    },
    {
        initialRouteName: 'landing',
    }
);

const Container = createAppContainer(Lander);

export default Container;