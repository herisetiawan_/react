import React, { Component } from 'react';
import { Alert, Text, TextInput, View, ImageBackground, TouchableHighlight, StatusBar } from 'react-native';
import { login, basic } from './src/styles/Styles';
import firebase from 'react-native-firebase';

export default class Login extends Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        email: '',
        password: '',
        errorMessage: '',
    }
    handleLogin = () => {
        const { email, password } = this.state
        const emailVer = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email !== '') {
            if (password !== '') {
                if (emailVer.test(email) === true) {
                    if (password.length >= 8) {
                        firebase
                            .auth()
                            .signInWithEmailAndPassword(email, password)
                            .then(Alert.alert('Sign In Success'))
                            .then(() => this.props.navigation.navigate('loading'))
                            .catch(error => this.setState({ errorMessage: error.message }))
                    }
                }
            } else {
                Alert.alert('Please Enter Password')
            }
        } else {
            Alert.alert('Please Enter Email')
        }
    }
    render() {
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode='cover' blurRadius={2}>
                <StatusBar backgroundColor='black' />
                <View style={login.container}>
                    <View style={login.headContainer}>
                        <Text style={basic.head}>
                            Welcome
                </Text>
                        <Text style={basic.headSub}>
                            We glad to see you again :D
                </Text>
                    </View>
                    <View style={basic.form}>
                        <Text style={basic.formLabel}>
                            Email or username :
                            </Text>
                        <TextInput style={basic.input}
                            placeholder='Enter email or username here'
                            placeholderTextColor='#cccccc'
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                        />
                        <Text style={basic.formLabel}>
                            Password :
                            </Text>
                        <TextInput style={basic.input}
                            placeholder='Enter password here'
                            placeholderTextColor='#cccccc'
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                            secureTextEntry
                        />
                    </View>
                    <View style={basic.submit}>
                        <TouchableHighlight style={basic.submitButton} onPress={this.handleLogin} underlayColor='#333'>
                            <View style={basic.submitTextCon}>
                                <Text style={basic.submitText}>
                                    Login
                                </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={basic.another}>
                        <Text style={basic.anotherText, { fontWeight: 'bold', color: 'white' }}>
                            Don't have an account?
                            </Text>
                        <TouchableHighlight onPress={() => this.props.navigation.navigate('register')} underlayColor='#333'>
                            <Text style={basic.anotherText}>
                                Create one now.
                                </Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}
