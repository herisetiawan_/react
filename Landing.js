import React, { Component } from 'react';
import { Text, View, ImageBackground, TouchableHighlight, StatusBar } from 'react-native';
import { landing, basic } from './src/styles/Styles';
import firebase from 'react-native-firebase';

export default class Landing extends Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        currentUser: '',
        button: 'Get Started',
        name: '',
    }
    componentDidMount() {
        const { currentUser } = firebase.auth()
        this.setState({ currentUser })
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.setState({
                    button: 'Go To Profile',
                })
            } else {
                this.setState({ name: 'YOU' })
            }
        })
    }
    getStartedLink = () => {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.props.navigation.navigate('profile')
            } else {
                this.props.navigation.navigate('login')
            }
        })
    }
    render() {
        const { currentUser, button, name } = this.state
        return (
            <ImageBackground source={require('./src/img/bg.jpg')} style={{ width: '100%', }}>
                <StatusBar backgroundColor='black' hidden />
                <View style={landing.container}>
                    <View style={landing.welcomeCon}>
                        <Text style={landing.welcome}>
                            Welcome
            </Text>
                        <Text style={landing.headerTextStyle}>
                            "{name}{currentUser && currentUser.displayName}"
                    </Text>
                    </View>
                    <View style={landing.descContainer}>
                        <Text style={landing.descText}>
                            Making simple way is possible. You can make it. Don't think you can't do anything. Just do it, i promise you can, you just need to attempt. Fail? no problem, you can try it again.
                    </Text>
                    </View>
                    <View style={landing.buttonCon}>
                        <TouchableHighlight style={landing.button} onPress={this.getStartedLink} underlayColor='#333'>
                            <Text style={landing.buttonText}>
                                {button}
                            </Text>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flexDirection: 'row-reverse', height: 30, paddingHorizontal: '5%' }}>
                        <TouchableHighlight style={{ borderWidth: 1, borderColor: 'white', borderRadius: 10, height: '100%', width: '20%', }} onPress={() => this.props.navigation.navigate('extras')} underlayColor='#333'>
                            <Text style={{ color: 'white', padding: '5%', textAlign: 'center' }}>
                                Extra
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}