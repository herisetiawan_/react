import React, { Component } from 'react';
import { View } from "react-native";
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Cacl from './src/components/calc2/Main';
import ToDo from './src/components/todo/Main';
import About from './src/components/profile/Main'
import Icon from 'react-native-vector-icons/Entypo';

const ExAct = createMaterialBottomTabNavigator(
    {
        calc: {
            screen: Cacl,
            navigationOptions: {
                tabBarLabel: 'Calc',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'user'} />
                    </View>),
            }
        },
        todo: {
            screen: ToDo,
            navigationOptions: {
                tabBarLabel: 'ToDo.',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'add-user'} />
                    </View>),
                activeColor: '#fff',
                inactiveColor: '#555',
                barStyle: { backgroundColor: '#222', },
            }
        },
        about: {
            screen: About,
            navigationOptions: {
                tabBarLabel: 'About',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'add-user'} />
                    </View>),
                activeColor: '#fff',
                inactiveColor: '#222',
                barStyle: { backgroundColor: '#555' },
            }
        },
    },
    {
        initialRouteName: "calc",
        activeColor: '#fff',
        inactiveColor: '#333',
        barStyle: { backgroundColor: '#a00' },
    },
);
const Extra = createAppContainer(ExAct);

export default class Extras extends Component {
    static navigationOptions = {
        header: null,
    }
    render() {
        return (
            <Extra />
        )
    }
}